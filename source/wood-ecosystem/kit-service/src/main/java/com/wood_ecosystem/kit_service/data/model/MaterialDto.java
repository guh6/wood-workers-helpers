package com.wood_ecosystem.kit_service.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */

@Getter
@Setter
public class MaterialDto {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("dimensions")
    private String dimensions;

    @JsonProperty("uuid")
    private String uuid;
}
