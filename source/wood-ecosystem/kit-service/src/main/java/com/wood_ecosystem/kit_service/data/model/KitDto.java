package com.wood_ecosystem.kit_service.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wood_ecosystem.kit_service.data.entity.KitMaterial;
import com.wood_ecosystem.kit_service.data.entity.Tag;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@Getter
@Setter
public class KitDto {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty
    private String uuid;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("verified")
    private boolean verified;

    @JsonProperty("category")
    private String category;

    @JsonProperty("difficulty")
    private String difficulty;

    @JsonProperty("created_at")
    private OffsetDateTime createdAt;

    @JsonProperty("modified_at")
    private OffsetDateTime modifiedAt;

    @JsonProperty("created_by")
    private String createdBy;

    @JsonProperty("tags")
    private List<Tag> tags;

    @JsonProperty("materials")
    private List<KitMaterial> materials;

}
