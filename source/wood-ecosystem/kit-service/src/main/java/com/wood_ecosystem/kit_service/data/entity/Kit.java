package com.wood_ecosystem.kit_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * @author guoyang on 9/29/21
 * @project wood-ecosystem
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "kit")
public class Kit {
    @Id
    @SequenceGenerator(name = "kit_seq_generator", sequenceName = "kit_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kit_seq_generator")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name="uuid")
    private String uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "verified")
    private boolean verified;

    @Column(name = "category")
    private String category;

    @Column(name = "difficulty")
    private String difficulty;

    @Column(name = "created_at")
    private OffsetDateTime createdAt;

    @Column(name = "modified_at")
    private OffsetDateTime modifiedAt;

    @Column(name = "created_by")
    private String createdBy;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "kit_tag",
               joinColumns = { @JoinColumn(name = "kit_id")},
               inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    private List<Tag> tags;

    @OneToMany(
            mappedBy = "kit",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<KitMaterial> kitMaterials;
}
