package com.wood_ecosystem.kit_service.services;

import com.wood_ecosystem.kit_service.data.mapstruct.mappers.MapStructMapper;
import com.wood_ecosystem.kit_service.data.model.KitDto;
import com.wood_ecosystem.kit_service.data.repositories.KitRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@Service
@AllArgsConstructor
public class KitRepositoryService {
    private final KitRepository kitRepository;
    private final MapStructMapper mapStructMapper;

    /**
     * See: https://stackoverflow.com/questions/34702252/spring-data-jpa-crudrepository-returns-iterable-is-it-ok-to-cast-this-to-list
     */
    public List<KitDto> getAllKits(){
        return mapStructMapper.kitsToKitDtos(Streamable.of(kitRepository.findAll()).toList());
    }
}
