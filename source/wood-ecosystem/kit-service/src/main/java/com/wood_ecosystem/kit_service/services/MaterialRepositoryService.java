package com.wood_ecosystem.kit_service.services;

import com.wood_ecosystem.kit_service.data.mapstruct.mappers.MapStructMapper;
import com.wood_ecosystem.kit_service.data.model.MaterialDto;
import com.wood_ecosystem.kit_service.data.repositories.MaterialRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@Service
@AllArgsConstructor
public class MaterialRepositoryService {
    private final MaterialRepository materialRepository;
    private final MapStructMapper mapStructMapper;

    public List<MaterialDto> getAll(){
        return mapStructMapper.materialsToMaterialDtos(Streamable.of(materialRepository.findAll()).toList());
    }
}
