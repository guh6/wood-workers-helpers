package com.wood_ecosystem.kit_service.rest.controllers;

import com.wood_ecosystem.kit_service.data.model.TagDto;
import com.wood_ecosystem.kit_service.services.TagRepositoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@RestController
@RequestMapping("/api/tags")
@AllArgsConstructor
public class TagController {

    private final TagRepositoryService tagRepositoryService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TagDto>> getAll(){
        return ResponseEntity.ok(tagRepositoryService.getAllTags());
    }
}
