package com.wood_ecosystem.kit_service.rest.controllers;

import com.wood_ecosystem.kit_service.data.model.KitDto;
import com.wood_ecosystem.kit_service.services.KitRepositoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@RestController
@RequestMapping("/api/kits")
@AllArgsConstructor
public class KitController {

    private final KitRepositoryService kitRepositoryService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KitDto>> getAll(){
        return ResponseEntity.ok(kitRepositoryService.getAllKits());
    }


}
