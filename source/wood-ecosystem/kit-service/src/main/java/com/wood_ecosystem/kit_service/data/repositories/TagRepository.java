package com.wood_ecosystem.kit_service.data.repositories;

import com.wood_ecosystem.kit_service.data.entity.Tag;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
public interface TagRepository extends PagingAndSortingRepository<Tag, Integer> {
}
