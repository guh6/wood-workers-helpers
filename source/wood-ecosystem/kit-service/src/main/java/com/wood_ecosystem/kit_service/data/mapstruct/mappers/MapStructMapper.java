package com.wood_ecosystem.kit_service.data.mapstruct.mappers;

import com.wood_ecosystem.kit_service.data.entity.Kit;
import com.wood_ecosystem.kit_service.data.entity.Material;
import com.wood_ecosystem.kit_service.data.entity.Tag;
import com.wood_ecosystem.kit_service.data.model.KitDto;
import com.wood_ecosystem.kit_service.data.model.MaterialDto;
import com.wood_ecosystem.kit_service.data.model.TagDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 *
 * See: https://auth0.com/blog/how-to-automatically-map-jpa-entities-into-dtos-in-spring-boot-using-mapstruct/
 */
@Mapper(
    componentModel = "spring"
)
public interface MapStructMapper {
    TagDto tagToTagDto(Tag tag);
    @Mapping(target = "kits", ignore = true)
    Tag tagDtoToTag(TagDto tagDto);

    List<TagDto> tagsToTagDtos(List<Tag> tags);
    List<Tag> tagDtosToTags(List<Tag> tagDtos);

    MaterialDto materialToMaterialDto(Material material);
    Material materialDtoToMaterial(MaterialDto materialDto);

    List<MaterialDto> materialsToMaterialDtos(List<Material> materials);
    List<Material> materialDtoToMaterials(List<MaterialDto> materialDtos);

    @Mapping(source = "kitMaterials", target = "materials")
    KitDto kitToKitDto(Kit kit);
    @Mapping(source = "materials", target = "kitMaterials")
    Kit kitDtoToKit(KitDto kitDto);

    List<KitDto> kitsToKitDtos(List<Kit> kits);
    List<Kit> kitDtosToKits(List<KitDto> kitDtos);
}
