package com.wood_ecosystem.kit_service.rest.controllers;

import com.wood_ecosystem.kit_service.data.model.MaterialDto;
import com.wood_ecosystem.kit_service.services.MaterialRepositoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@RestController
@RequestMapping("/api/materials")
@AllArgsConstructor
public class MaterialController {

    private final MaterialRepositoryService materialRepositoryService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MaterialDto>> getAll(){
        return ResponseEntity.ok(materialRepositoryService.getAll());
    }

}
