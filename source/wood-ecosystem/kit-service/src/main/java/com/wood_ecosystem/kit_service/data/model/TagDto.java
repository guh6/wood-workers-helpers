package com.wood_ecosystem.kit_service.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@Getter
@Setter
public class TagDto {
    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;
}
