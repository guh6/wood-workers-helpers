package com.wood_ecosystem.kit_service.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * @author guoyang
 */
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "tag")
public class Tag {
    @Id
    @SequenceGenerator(name = "kit_seq_generator", sequenceName = "kit_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kit_seq_generator")
    @Column(name = "id", updatable = false)
    private Integer id;

    private String name;

    @JsonIgnore // StackOverflow
    @ManyToMany(mappedBy = "tags")
    private Set<Kit> kits;
}
