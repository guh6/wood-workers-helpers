package com.wood_ecosystem.kit_service.data.repositories;

import com.wood_ecosystem.kit_service.data.entity.Kit;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
public interface KitRepository extends PagingAndSortingRepository<Kit, Integer> {
}
