package com.wood_ecosystem.kit_service.services;

import com.wood_ecosystem.kit_service.data.mapstruct.mappers.MapStructMapper;
import com.wood_ecosystem.kit_service.data.model.TagDto;
import com.wood_ecosystem.kit_service.data.repositories.TagRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author guoyang on 10/2/21
 * @project wood-ecosystem
 */
@Service
@AllArgsConstructor
public class TagRepositoryService {
    private final TagRepository tagRepository;
    private final MapStructMapper mapStructMapper;

    public List<TagDto> getAllTags(){
        return mapStructMapper.tagsToTagDtos(Streamable.of(tagRepository.findAll()).toList());
    }

}
