package com.wood_ecosystem.kit_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author guoyang
 * @project wood-ecosystem
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "material")
public class Material {
    @Id
    @SequenceGenerator(name = "material_seq_generator", sequenceName = "material_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "material_seq_generator")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String dimensions;

    @Column
    private String uuid;
}
