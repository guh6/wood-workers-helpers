package com.wood_ecosystem.kit_service.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author guoyang on 9/29/21
 * @project wood-ecosystem
 *  * See: https://vladmihalcea.com/the-best-way-to-map-a-many-to-many-association-with-extra-columns-when-using-jpa-and-hibernate/
 */
@Getter
@Setter
@Entity(name = "kit_material")
public class KitMaterial {
    @Getter
    @Setter
    @NoArgsConstructor
    @Embeddable
    public static class KitMaterialId implements Serializable {
        private static final long serialVersionUID = -1798070786993154676L;

        @Column(name = "kit_id")
        private Integer kitId;

        @Column(name = "material_id")
        private Integer materialId;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            KitMaterialId that = (KitMaterialId) o;
            return Objects.equals(kitId, that.kitId) && Objects.equals(materialId, that.materialId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(kitId, materialId);
        }
    }

    @EmbeddedId
    private KitMaterialId kitMaterialId;

    @JsonIgnore // StackOverflow
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("kit_id")
    private Kit kit;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("material_id")
    private Material material;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "required")
    private boolean required;

}
