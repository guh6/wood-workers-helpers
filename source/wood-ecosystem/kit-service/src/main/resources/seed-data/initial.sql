SET search_path to kit_app;

INSERT INTO kit(id, name, description, verified, category, difficulty, created_at, created_by, uuid)
    VALUES(1, 'Simple Bird House', 'Create a bird house for beginners', true, 'outdoor', 'easy', now(), 'system', '0c50b711-91ef-45fd-9d22-810908fed9d0');

INSERT INTO tag(id, name)
    VALUES(1, 'outdoor'),
          (2, 'bird'),
          (3, 'nature');

INSERT INTO material(id, name, dimensions, description, uuid)
    VALUES(1, 'Roof Panel',  '1/2" x 2" x 4"', 'Pressure Treated 1/2" thick board', 'efaa9d2e-95c9-4dc1-974d-c88fb5e0af34'),
          (2, 'Side Panel',  '1/2" x 2" x 4"', 'Pressure Treated 1/2" thick board', '147ca647-2e6c-4325-afe3-c1b99169424a'),
          (3, 'Front Panel', '1/2" x 2" x 4"', 'Pressure Treated 1/2" thick board with door opening', 'a5cf8a7d-e96b-4cc6-b5e7-9b3b322a999d');


INSERT INTO kit_tag(kit_id, tag_id)
    VALUES(1, 1), (1,2), (1,3);

INSERT INTO kit_material(kit_id, material_id, quantity, required)
    VALUES(1, 1, 2, true),
          (1, 2, 4, true),
          (1, 3, 1, true);
