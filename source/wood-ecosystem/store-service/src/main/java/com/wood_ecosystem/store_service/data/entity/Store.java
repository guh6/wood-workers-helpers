package com.wood_ecosystem.store_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "store")
public class Store {
    @Id
    @SequenceGenerator(name = "store_seq_generator", sequenceName = "store_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "store_seq_generator")
    private Integer id;

    @Column(name = "identifier")
    private String identifier;

    @Column(name = "address")
    private String address;

    @Column(name = "zipcode")
    private String zipcode;

    @Column(name = "state")
    private String state;

    @Column(name = "created_at")
    private OffsetDateTime createdAt;

    @Column(name = "modified_at")
    private OffsetDateTime modifiedAt;
}
