package com.wood_ecosystem.inventory_service.services;

import com.wood_ecosystem.inventory_service.data.mapstruct.mappers.MapStructMapper;
import com.wood_ecosystem.inventory_service.data.model.StoreInventoryDto;
import com.wood_ecosystem.inventory_service.data.repositories.StoreInventoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author guoyang on 10/24/21
 * @project wood-ecosystem
 */
@Service
@AllArgsConstructor
public class InventoryService {
    private final StoreInventoryRepository storeInventoryRepository;
    private final MapStructMapper mapStructMapper;

    /**
     * See https://ozenero.com/reactor-create-flux-and-mono-simple-ways-to-create-publishers-reactive-programming
     * @return
     */
    public Flux<StoreInventoryDto> getAll(){
        List<StoreInventoryDto> storeInventoryDtoList = Streamable.of(storeInventoryRepository.findAll()).toList()
                .stream().map(mapStructMapper::entityToDto).collect(Collectors.toList());

        return Flux.fromIterable(storeInventoryDtoList);
    }

}

