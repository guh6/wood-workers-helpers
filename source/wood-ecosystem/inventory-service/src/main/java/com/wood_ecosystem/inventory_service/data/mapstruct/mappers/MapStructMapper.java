package com.wood_ecosystem.inventory_service.data.mapstruct.mappers;

import com.wood_ecosystem.inventory_service.data.entity.StoreInventory;
import com.wood_ecosystem.inventory_service.data.model.StoreInventoryDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author guoyang on 10/24/21
 * @project wood-ecosystem
 */
@Mapper(
        componentModel = "spring"
)
public interface MapStructMapper {
    StoreInventoryDto entityToDto(StoreInventory storeInventory);
    List<StoreInventoryDto> entityToDto(List<StoreInventory> storeInventories);
}
