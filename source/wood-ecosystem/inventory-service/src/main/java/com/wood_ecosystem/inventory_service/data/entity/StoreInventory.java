package com.wood_ecosystem.inventory_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "store_inventory")
public class StoreInventory {
    @Id
    @SequenceGenerator(name = "store_inventory_seq_generator", sequenceName = "store_inventory_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "store_inventory_seq_generator")
    private Long id;

    @Column(name = "store_identifier")
    private String storeIdentifier;

    @Column(name = "material_identifier")
    private String materialIdentifier;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "created_at")
    private OffsetDateTime createdAt;

    @Column(name = "modified_at")
    private OffsetDateTime modifiedAt;
}
