package com.wood_ecosystem.inventory_service.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author guoyang on 10/24/21
 * @project wood-ecosystem
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StoreInventoryDto {
    private String storeIdentifier;
    private String materialIdentifier;
    private long quantity;
}
