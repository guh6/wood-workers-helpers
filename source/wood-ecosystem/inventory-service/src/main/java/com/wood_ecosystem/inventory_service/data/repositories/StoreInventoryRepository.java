package com.wood_ecosystem.inventory_service.data.repositories;

import com.wood_ecosystem.inventory_service.data.entity.StoreInventory;
import org.springframework.data.repository.CrudRepository;

/**
 * @author guoyang on 10/24/21
 * @project wood-ecosystem
 */
public interface StoreInventoryRepository extends CrudRepository<StoreInventory, Long> {
}
