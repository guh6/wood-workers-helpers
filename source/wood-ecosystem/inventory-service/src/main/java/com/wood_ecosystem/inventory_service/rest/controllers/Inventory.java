package com.wood_ecosystem.inventory_service.rest.controllers;

import com.wood_ecosystem.inventory_service.data.model.StoreInventoryDto;
import com.wood_ecosystem.inventory_service.services.InventoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @author guoyang on 10/24/21
 * @project wood-ecosystem
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/inventories")
@AllArgsConstructor
public class Inventory {
    private final InventoryService inventoryService;

    @GetMapping()
    public Flux<StoreInventoryDto> getAllInventory() {
        return inventoryService.getAll();
    }

}
