SET search_path to inventory_app;

CREATE SEQUENCE IF NOT EXISTS store_inventory_seq START 1 INCREMENT BY 1 CACHE 1;

CREATE TABLE store_inventory (
	id BIGINT PRIMARY KEY DEFAULT NEXTVAL('store_inventory_seq'),
	store_identifier VARCHAR(36) NOT NULL,
	material_identifier VARCHAR(36) NOT NULL,
	quantity BIGINT,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
	modified_at TIMESTAMP WITH TIME ZONE,
	UNIQUE(store_identifier, material_identifier)
);