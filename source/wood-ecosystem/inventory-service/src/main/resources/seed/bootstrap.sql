SET search_path to inventory_app;

INSERT INTO store_inventory (store_identifier, material_identifier, quantity) VALUES
	('0acfee2c-fa68-43f2-864e-a83caffcabea', 'd9c8c4ee-1c04-46d8-b618-1e56fcec7dd8', 10), -- Store A, Bird House Roof Slats
	('0acfee2c-fa68-43f2-864e-a83caffcabea', '396ba6d6-b978-4fce-bead-504645e5e992', 10), -- Store A, Bird House Base
	('0acfee2c-fa68-43f2-864e-a83caffcabea', 'e0e063d6-9ce4-4adf-afa9-2f61f82ab279', 10); -- Store A, Bird House Sides