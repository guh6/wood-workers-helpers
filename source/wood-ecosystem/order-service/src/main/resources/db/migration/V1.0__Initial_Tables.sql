SET search_path to order_app;

CREATE SEQUENCE IF NOT EXISTS order_seq START 1 INCREMENT BY 1 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS payment_seq START 1 INCREMENT BY 1 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS purchaser_seq START 1 INCREMENT BY 1 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS order_line_seq START 1 INCREMENT BY 1 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS material_line_details_seq START 1 INCREMENT BY 1 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS order_line_kit_details_seq START 1 INCREMENT BY 1 CACHE 1;


CREATE TABLE "order" (
	id BIGINT PRIMARY KEY DEFAULT NEXTVAL('order_seq'),
	identifier VARCHAR(36) UNIQUE NOT NULL,
	store_identifier VARCHAR(36) NOT NULL,
    status VARCHAR(10) NOT NULL,
    amount NUMERIC NOT NULL,
    tax NUMERIC NOT NULL,
    total_amount NUMERIC NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
	modified_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE purchaser (
	id BIGINT PRIMARY KEY DEFAULT NEXTVAL('purchaser_seq'),
	user_id BIGINT,
	order_id BIGINT NOT NULL,
	first_name VARCHAR(100) NOT NULL,
	middle_name VARCHAR(100),
	last_name VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL
);
ALTER TABLE purchaser ADD CONSTRAINT purchaser_order_id FOREIGN KEY (order_id) REFERENCES "order"(id) ON DELETE CASCADE;

CREATE TABLE payment (
	id BIGINT PRIMARY KEY DEFAULT NEXTVAL('payment_seq'),
	order_id BIGINT NOT NULL,
	status VARCHAR(10) NOT NULL default('new'),
	paid_amount NUMERIC,
	billing_address VARCHAR(256) NOT NULL,
	billing_zipcode VARCHAR(20) NOT NULL,
	quantity INTEGER NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
	modified_at TIMESTAMP WITH TIME ZONE
);
ALTER TABLE payment ADD CONSTRAINT payment_order_id FOREIGN KEY (order_id) REFERENCES "order"(id) ON DELETE CASCADE;

CREATE TABLE order_line (
    id BIGINT PRIMARY KEY DEFAULT NEXTVAL('order_line_seq'),
    order_id BIGINT NOT NULL,
    kit_identifier VARCHAR(36) NOT NULL,
    material_identifier VARCHAR(36) NOT NULL,
    cost NUMERIC NOT NULL,
    quantity INTEGER NOT NULL,
    status VARCHAR(10) NOT NULL DEFAULT('pending'),
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    modified_at TIMESTAMP WITH TIME ZONE
);
ALTER TABLE order_line ADD CONSTRAINT order_line_order_id_fk FOREIGN KEY (order_id) REFERENCES "order"(id) ON DELETE CASCADE;

CREATE TABLE order_line_kit_details (
    id BIGINT PRIMARY KEY DEFAULT NEXTVAL('order_line_kit_details_seq'),
    order_line_id BIGINT NOT NULL,
    name VARCHAR(256) NOT NULL,
    description VARCHAR(256) NOT NULL
);
ALTER TABLE order_line_kit_details ADD CONSTRAINT order_line_kit_details_order_line_id_fk FOREIGN KEY (order_line_id)
    REFERENCES order_line(id) ON DELETE CASCADE;

CREATE TABLE material_line_details (
    id BIGINT PRIMARY KEY DEFAULT NEXTVAL('material_line_details_seq'),
    order_line_id BIGINT NOT NULL,
    name VARCHAR(256) NOT NULL,
    description VARCHAR(256) NOT NULL
);
ALTER TABLE material_line_details ADD CONSTRAINT material_line_details_order_line_id_fk FOREIGN KEY (order_line_id)
    REFERENCES order_line(id) ON DELETE CASCADE;