package com.wood_ecosystem.order_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Entity(name = "order_line")
@Getter
@Setter
@NoArgsConstructor
public class OrderLine {
    @Id
    @SequenceGenerator(name = "payment_seq_generator", sequenceName = "payment_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_seq_generator")
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "kit_identifier")
    private String kitIdentifier;

    @Column(name = "material_identifier")
    private String materialIdentifier;

    @Column(name = "cost")
    private BigDecimal cost;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "status")
    private String status;

    @Column(name = "created_at")
    private OffsetDateTime createdAt;

    @Column(name = "modified_at")
    private OffsetDateTime modifiedAt;
}

//    @Column(name = "")
