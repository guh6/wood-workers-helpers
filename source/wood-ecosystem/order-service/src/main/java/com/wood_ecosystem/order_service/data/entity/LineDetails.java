package com.wood_ecosystem.order_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Getter
@Setter
@NoArgsConstructor
public class LineDetails {
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;
}
