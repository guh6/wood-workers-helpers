package com.wood_ecosystem.order_service.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Entity(name = "order")
@Getter
@Setter
@NoArgsConstructor
public class Order {
    @Id
    @SequenceGenerator(name = "order_seq_generator", sequenceName = "order_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq_generator")
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "identifier")
    private String identifier;

    @Column(name = "store_identifier")
    private String storeIdentifier;

    @Column(name = "status")
    private String status;

    @Column(name = "amount", scale = 2, precision = 2)
    private BigDecimal amount;

    @Column(name = "tax", scale = 2, precision = 2)
    private BigDecimal tax;

    @Column(name = "total_amount", scale = 2, precision = 2)
    private BigDecimal totalAmount;

    @Column(name = "created_at")
    private OffsetDateTime createdAt;

    @Column(name = "modified_at")
    private OffsetDateTime modifiedAt;
}
