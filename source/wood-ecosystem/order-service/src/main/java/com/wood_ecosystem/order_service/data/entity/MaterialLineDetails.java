package com.wood_ecosystem.order_service.data.entity;

import javax.persistence.*;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Entity(name = "material_line_details")
public class MaterialLineDetails extends LineDetails {
    @Id
    @SequenceGenerator(name = "material_line_details_seq_generator", sequenceName = "material_line_details_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "material_line_details_seq_generator")
    @Column(name = "id", updatable = false)
    private Long id;

}
