package com.wood_ecosystem.order_service.data.entity;

import javax.persistence.*;

/**
 * @author guoyang on 10/9/21
 * @project wood-ecosystem
 */
@Entity(name = "order_line_kit_details")
public class OrderLineKitDetails extends LineDetails{
    @Id
    @SequenceGenerator(name = "order_line_kit_details_seq_generator", sequenceName = "order_line_kit_details_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_line_kit_details_seq_generator")
    @Column(name = "id", updatable = false)
    private Long id;
}
